<b>proto objects</b> - src/main/proto

Building.proto (Proto object for building table)

Employee.proto (Proto object for employee table)

Attendance.proto(Proto object for Attendance table)

<b>Attendance csv data</b> - src/main/resources/

<b> Attendance Proto Output File</b>- src/main/resources/ProtoOutputFile

<b> Minimum Attendance Employee Output</b> - src/main/resources/ProtoOutputFile/OutPut/

Constant data has been configured inside Util Package under Constants.java Program.

<b>Following steps has been used to create this program.</b>

1. Added required hadoop, protobuff dependencies inside build.graddle file.
2. Created CSVtoProto.java file to create Attendance serialized file using protobuff.(path - src/main/java/CSVtoProto.java).
3. Uploaded Attendance data in HDFS using UploadFiletoHDFS.java code.(path - src/main/java/UploadFiletoHDFS.java)
4. Created 'attendanceData' table in Hbase using HbaseTableCreator.java code.(path - src/main/java/HbaseTableCreator.java)
5. Uploaded attendance data in 'attendanceData table' from HDFS.(Code used :src/main/java/MRDriver.java, src/main/java/HbaseMapper.java,src/main/java/WholeFileInputFormat.java)
6. Created File 'EmployeeAttendanceDriver.java' to fetch each building employee with the lowest attendance.(path - src/main/java/EmployeeAttendanceDriver.java)
