// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Attendance.proto

package ProtoFiles.Attendance;

public interface AttendanceListOrBuilder extends
    // @@protoc_insertion_point(interface_extends:ProtoFiles.Attendance.AttendanceList)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated .ProtoFiles.Attendance.Attendance attendance = 1;</code>
   */
  java.util.List<ProtoFiles.Attendance.Attendance> 
      getAttendanceList();
  /**
   * <code>repeated .ProtoFiles.Attendance.Attendance attendance = 1;</code>
   */
  ProtoFiles.Attendance.Attendance getAttendance(int index);
  /**
   * <code>repeated .ProtoFiles.Attendance.Attendance attendance = 1;</code>
   */
  int getAttendanceCount();
  /**
   * <code>repeated .ProtoFiles.Attendance.Attendance attendance = 1;</code>
   */
  java.util.List<? extends ProtoFiles.Attendance.AttendanceOrBuilder> 
      getAttendanceOrBuilderList();
  /**
   * <code>repeated .ProtoFiles.Attendance.Attendance attendance = 1;</code>
   */
  ProtoFiles.Attendance.AttendanceOrBuilder getAttendanceOrBuilder(
      int index);
}
