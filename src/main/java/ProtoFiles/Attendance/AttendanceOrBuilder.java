// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Attendance.proto

package ProtoFiles.Attendance;

public interface AttendanceOrBuilder extends
    // @@protoc_insertion_point(interface_extends:ProtoFiles.Attendance.Attendance)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>int32 Employee_Id = 1;</code>
   * @return The employeeId.
   */
  int getEmployeeId();

  /**
   * <code>string Date = 2;</code>
   * @return The date.
   */
  java.lang.String getDate();
  /**
   * <code>string Date = 2;</code>
   * @return The bytes for date.
   */
  com.google.protobuf.ByteString
      getDateBytes();
}
