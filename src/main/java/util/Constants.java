package util;

public class Constants {
    public static final String AttendanceCSVFilePath= "/Users/sakshi/IdeaProjects/Capstone-Project-4c/src/main/resources/Attendance.csv";
    public static final String AttendanceProtoOutputFile="/Users/sakshi/IdeaProjects/Capstone-Project-4c/src/main/resources/ProtoOutputFile/Attendance";
    public static final String uri = "hdfs://localhost:9000";
    public static final String Attendance_HDFS_OUTPUT_PATH=uri+"/atendanceSerializeFile";;
    public static final String Attendance_TABLE_NAME="attendanceData";
    //public static final String ATTENDANCE = "attendance";
    public static final String Attendance_DETAILS = "attendance_details";
    public static final String COLUMN_FAMILY_Attendance = "attendance";
    public static final String TABLE_ALREADY_EXISTS = "Table already exists";
    public static final String TABLE_CREATED =  "table created";
    public static final String Attendance_HFILE_OUTPUT_PATH = "hdfs://localhost:9000/Hfile/attendance_hfile/";
    public static final String DEFAULT_FS = "fs.defaultFS";
    public static final String HDFS_INPUT_URL = "hdfs://localhost:9000/";
    public static final String HDFS_IMPL = "fs.hdfs.impl";
    public static final String FILE_IMPL = "fs.file.impl";
    public static final String BULK_LOADING_MESSAGE = "Bulk Loading HBase Table::";
    public final static String employeeTableName = "employeeData";
    public final static String buildingTableName = "buildingData";
    public final static String employeeColumnFamily = "employee";
    public final static String buildingColumnFamily = "building";
    public final static String columnName = "ProtoObject";
}
