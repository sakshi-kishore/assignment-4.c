import static util.Constants.*;
import ProtoFiles.Attendance.Attendance;
import ProtoFiles.Attendance.AttendanceList;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

import java.util.*;
import java.util.stream.Collectors;

class AttendanceMapper extends Mapper<NullWritable, BytesWritable, ImmutableBytesWritable, Put> {

    ImmutableBytesWritable TABLE_NAME_TO_INSERT = new ImmutableBytesWritable(Bytes.toBytes(Attendance_TABLE_NAME));

    public void map(NullWritable key, BytesWritable value, Context context) {
        try {

            byte b[] = value.getBytes();
            AttendanceList attendanceList = AttendanceList.parseFrom(Arrays.copyOf(value.getBytes(), value.getLength()));
            int i = 0;
            for(int j=0; j<100; j++) {
                int empId = j;
                List<Attendance> newList = attendanceList.getAttendanceList()
                        .stream()
                        .filter(m -> m.getEmployeeId() == empId)
                        .collect(Collectors.toList());
                AttendanceList dbo = AttendanceList.newBuilder().addAllAttendance(newList).build();
                if(newList.size() > 0) {
                    Put put = new Put(Bytes.toBytes(String.valueOf(i+"")));
                    put.addColumn(Bytes.toBytes(COLUMN_FAMILY_Attendance), Bytes.toBytes("proto_obj"),
                            dbo.toByteArray());

                    context.write(TABLE_NAME_TO_INSERT, put);
                    i++;
                }
            }



        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
