import static util.Constants.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import java.io.*;

public class HbaseTableCreator {
    public void createTableAttendnce(String tableNameToCreate) throws IOException {

        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Admin hAdmin = connection.getAdmin();
        if (hAdmin.tableExists(TableName.valueOf(tableNameToCreate))) {
            System.out.println(TABLE_ALREADY_EXISTS);
            return;
        }
        TableName tname = TableName.valueOf(tableNameToCreate);
        TableDescriptorBuilder tableDescBuilder = TableDescriptorBuilder.newBuilder(tname);

        ColumnFamilyDescriptorBuilder columnDescBuilderBuilding = ColumnFamilyDescriptorBuilder
                .newBuilder(Bytes.toBytes(COLUMN_FAMILY_Attendance));
        tableDescBuilder.setColumnFamily(columnDescBuilderBuilding.build());

        tableDescBuilder.build();

        hAdmin.createTable(tableDescBuilder.build());
        System.out.println(tableNameToCreate + TABLE_CREATED);
    }

}