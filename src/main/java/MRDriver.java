import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.tool.BulkLoadHFiles;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import static util.Constants.*;

import java.io.IOException;
import java.net.URI;

public class MRDriver {

    public static final String Attendance_HDFS_INPUT_PATH = Attendance_HDFS_OUTPUT_PATH;

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Class<AttendanceMapper> attendanceMapperClass = AttendanceMapper.class;
        driver(Attendance_TABLE_NAME, Attendance_HDFS_INPUT_PATH, Attendance_HFILE_OUTPUT_PATH, attendanceMapperClass);
    }

    public static void driver(String tableToInsert, String hdfsInputPath, String hfileOutputPath, Class<AttendanceMapper> attendanceMapperClass) throws IOException, InterruptedException, ClassNotFoundException {

        Configuration configuration = new Configuration();
        configuration.set(DEFAULT_FS, HDFS_INPUT_URL);
        configuration.set(HDFS_IMPL, org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        configuration.set(FILE_IMPL, org.apache.hadoop.fs.LocalFileSystem.class.getName());

        Path output = new Path(hfileOutputPath);
        FileSystem hdfs = FileSystem.get(URI.create(hfileOutputPath), configuration);

        if (hdfs.exists(output)) {                                      // delete existing directory
            hdfs.delete(output, true);
        }
        Job job = Job.getInstance(configuration);

        job.setJarByClass(MRDriver.class);
        job.setJobName(BULK_LOADING_MESSAGE + tableToInsert);
        job.setInputFormatClass(WholeFileInputFormat.class);
        FileInputFormat.setInputPaths(job, hdfsInputPath);

        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapperClass(attendanceMapperClass);

        FileOutputFormat.setOutputPath(job, new Path(hfileOutputPath));
        job.setMapOutputValueClass(Put.class);

        TableName tableName = TableName.valueOf(tableToInsert);
        try (Connection conn = ConnectionFactory.createConnection(configuration); Table table = conn.getTable(tableName); RegionLocator regionLocator = conn.getRegionLocator(tableName)) {
            HFileOutputFormat2.configureIncrementalLoad(job, table, regionLocator);
        }

        boolean b = job.waitForCompletion(true);
        System.out.println(b);

        if (job.isSuccessful()) {

            //It bulk uploads data into the the table
            try {
                Configuration config = HBaseConfiguration.create();
                BulkLoadHFiles.create(config).bulkLoad(tableName, new Path(hfileOutputPath));
                System.out.println("successful upload to " + tableToInsert + " table");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
