import ProtoFiles.Attendance.Attendance;
import ProtoFiles.Attendance.AttendanceList;
import ProtoFiles.Employee.Employee;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.HashMap;

import static util.Constants.*;

public class EmployeeAttendanceDriver extends Configured implements Tool {


    public static class EmployeeAttendanceMapper extends TableMapper<ImmutableBytesWritable, Result> {
        HashMap<Integer, String> employeeBuildingInfo = new HashMap<>();

        @Override
        protected void setup(Mapper.Context context) throws IOException {
            Connection hbaseCon = ConnectionFactory.createConnection(new HBaseConfiguration().create());
            Table table = hbaseCon.getTable(TableName.valueOf(Bytes.toBytes(employeeTableName)));
            Scan scan = new Scan();
            scan.addFamily(Bytes.toBytes(employeeColumnFamily));
            scan.setCaching(20);
            System.out.println("mapper setup");
            ResultScanner scanner = table.getScanner(scan);

            for (Result result = scanner.next(); (result != null); result = scanner.next()) {
                byte[] cf = Bytes.toBytes("employee");
                byte[] cqId = Bytes.toBytes("employee_id");
                byte[] cqBuilding = Bytes.toBytes("building_code");
                int employeeId = Integer.parseInt(Bytes.toString(result.getValue(cf, cqId)));
                Employee emp = Employee.newBuilder().setEmployeeId(employeeId)
                        .setBuildingCode(Bytes.toString(result.getValue(cf, cqBuilding)))
                        .build();
                employeeBuildingInfo.put(emp.getEmployeeId(), emp.getBuildingCode());
            }
        }

        public void map(ImmutableBytesWritable key, Result value, Mapper.Context context)
                throws IOException, InterruptedException {

            byte[] valueArray = value.getValue(Bytes.toBytes(COLUMN_FAMILY_Attendance), Bytes.toBytes("proto_obj"));
            System.out.println("mapper method called");
            AttendanceList dbo = AttendanceList.parseFrom(valueArray);
            //Attendance dbo = Attendance.parseFrom(valueArray);
            System.out.println("count: " + dbo.getAttendanceCount());
            if(dbo.getAttendanceCount() > 0) {
                int employeeId = dbo.getAttendance(0).getEmployeeId();
                String emp = Integer.toString(employeeId);
                if (!emp.equals("Employee_id")) {
                    Text outKey = new Text(employeeBuildingInfo.get(employeeId));
                    int totalAttendance = dbo.getAttendanceCount();
                    // dbo.getAttendanceCount();dbo.getDate().split("#").length;
                    String totalAttendanceInfo = totalAttendance + "," + employeeId;
                    Text outValue = new Text(totalAttendanceInfo);
                    context.write(outKey, outValue);
                }
            }
        }
    }

    public static class EmployeeAttendanceReducer extends Reducer<Text, Text, Text, Text> {

        @Override
        public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            int minAttendance = 1000;
            String minEmployeeId = "";
            System.out.println("reducer");

            for (Text val1 : values) {
                int currentAttendance = Integer.parseInt(val1.toString().split(",")[0]);
                String currentEmployeeId = val1.toString().split(",")[1];
                if (currentAttendance < minAttendance) {
                    minAttendance = currentAttendance;
                    minEmployeeId = currentEmployeeId;
                }
            }
            context.write(key, new Text(": \tEmployee Id: " + minEmployeeId
                    + "\tCount: " + minAttendance));

        }
    }

    public int run(String[] args) throws Exception {
        Scan scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        job.setJarByClass(EmployeeAttendanceDriver.class);
        TableMapReduceUtil.initTableMapperJob(TableName.valueOf(Attendance_TABLE_NAME),
                scan, EmployeeAttendanceMapper.class,
                Text.class, Text.class, job);

        job.setReducerClass(EmployeeAttendanceReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        Path path = new Path("/Users/sakshi/IdeaProjects/Capstone-Project-4c/src/main/resources/ProtoOutputFile/OutPut");
        FileOutputFormat.setOutputPath(job, path);
        boolean b = job.waitForCompletion(true);
        System.out.println(b);

        return 0;
    }

    public static void main(String[] args) throws Exception {

        EmployeeAttendanceDriver runJob = new EmployeeAttendanceDriver();
        runJob.run(args);
    }

}
