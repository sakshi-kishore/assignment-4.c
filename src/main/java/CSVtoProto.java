import ProtoFiles.Attendance.AttendanceList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import util.Constants;
public class CSVtoProto {
    public void  readCsvData(String FilePath)
    {
        try {

            FileReader fr = new FileReader(FilePath);
            BufferedReader br = new BufferedReader(fr);
            String line = "" ;
            boolean header = true;

            while((line = br.readLine()) != null)
            {
                if (header) {
                    // avoiding first line of csv file
                    header = false;
                    continue;
                }
                String [] data = line.split(",");
                printAttendanceData(data);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void printAttendanceData(String attendanceData[])
    {
        if(attendanceData.length==0)
            return;
        ProtoFiles.Attendance.Attendance.Builder attendance =ProtoFiles.Attendance.Attendance.newBuilder();
        attendance.setEmployeeId(Integer.parseInt(attendanceData[0]))
                .setDate(attendanceData[1]);
        System.out.println(attendance.toString());
        AttendanceList.Builder attendanceList=AttendanceList.newBuilder();
        try {
            attendanceList.mergeFrom(new FileInputStream(Constants.AttendanceProtoOutputFile));
        } catch (FileNotFoundException e) {
            System.out.println(Constants.AttendanceProtoOutputFile + ": File not found.  Creating a new file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        attendanceList.addAttendance(attendance.build());
        try (FileOutputStream output = new FileOutputStream(Constants.AttendanceProtoOutputFile)) {
            attendanceList.build().writeTo(output);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void main(String args[])
    {
        CSVtoProto obj=new CSVtoProto();
        obj.readCsvData(Constants.AttendanceCSVFilePath);

    }

}
